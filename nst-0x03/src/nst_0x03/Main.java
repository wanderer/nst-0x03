package nst_0x03;


import java.net.*;
import java.util.concurrent.*;
import java.io.*; 

class socket_handler {
	Socket s;
	OutputStream os;
	output_handler oh;
	static ExecutorService executor = Executors.newCachedThreadPool();
	public socket_handler(Socket socket) throws IOException {
		s = socket;
		try {
			OutputStream os = s.getOutputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		executor.execute(new input_handler());
		oh = new output_handler();
		executor.execute(oh);
	}
	
	class input_handler implements Runnable {
		public void run() {
			try {
				InputStream is  = s.getInputStream();
				os = s.getOutputStream();
				InetAddress ia = InetAddress.getLocalHost();
				String hostname = ia.getHostName();
				os.write(("* " + hostname + " says hi!\nplease, be nice\n").getBytes());

				int ch;
				while ( ((ch = is.read()) != -1) ) {
					System.out.write(("* client sent:" + (char)ch + "\n").getBytes());
					os.write(("* you have sent: " + (char)ch + "\n"+"* of which byte representation is: " + ch + "\n").getBytes());
					os.flush();
				}
				s.close();
				System.out.write(("* the connection has been closed\n").getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	class output_handler implements Runnable {
		public void run() {
			try {
				OutputStream os = s.getOutputStream();
				while (true) {
					if (s.isClosed()) {
						System.out.println("* client has closed connection");
						break;
					}
					Thread.sleep(15000);
					os.write((" [*] Doing really evil stuff\n").getBytes());
					os.flush();
				}
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}


public class Main {

	public static void main(String[] args) {
		try {
			int port = 1982;
			ServerSocket sesock = new ServerSocket(port);
			InetAddress ia = InetAddress.getLocalHost();
			System.out.println("* server started\n* listening on " + ia.getHostAddress() + ":" + port + "\n");
			while (true) {
				Socket s = sesock.accept();
				System.out.println("* new client\tip: " + s.getInetAddress() + "\trport: " + s.getPort());
				new socket_handler(s);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
